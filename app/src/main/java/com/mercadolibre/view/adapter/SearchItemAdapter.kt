package com.mercadolibre.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mercadolibre.repo.search.Item
import com.mercadolibre.R
import com.mercadolibre.view.SearchFragmentDirections


/**
 * Adapter between the items data and the recycleview.
 * This is pagedListAdapter capable of paginate items
 */
class SearchItemAdapter(private val context: Context) :
    PagedListAdapter<Item, SearchItemAdapter.ViewHolder>(POST_COMPARATOR) {

    class ViewHolder(private val context: Context, val view: View) : RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.title)
        private val thumbnail: ImageView = view.findViewById(R.id.thumbnail)
        private val value: TextView = view.findViewById(R.id.value)
        private val installments: TextView = view.findViewById(R.id.installments)

        fun bind(item: Item) {
            title.text = item.title

            val options = RequestOptions()
                .optionalFitCenter()
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)

            //TODO small hack to get more enhanced image
            Glide.with(context).load(item.thumbnail?.replace("-I.jpg", "-O.jpg")).apply(options).into(thumbnail)

            value.text = item.getPriceDescription()

            installments.text = item.getInstallmentsDescription()

            view.setOnClickListener {
                val nav = Navigation.findNavController(view)

                val navOption = NavOptions.Builder().setEnterAnim(R.anim.nav_default_enter_anim)
                    .setExitAnim(R.anim.nav_default_exit_anim)
                    .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
                    .setPopExitAnim(R.anim.nav_default_pop_exit_anim)
                    .build()

                val action = SearchFragmentDirections.actionSearchFragmentToDetailActivity(item)
                nav.navigate(action, navOption)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.ml_list_item, parent, false)
        return ViewHolder(context, view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        this.getItem(position)?.let {
            viewHolder.bind(it)
        }
    }

    companion object {
        val POST_COMPARATOR = object : DiffUtil.ItemCallback<Item>() {
            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean =
                oldItem.id == newItem.id

            override fun getChangePayload(oldItem: Item, newItem: Item): Any? {
                return newItem
            }
        }
    }

}