package com.mercadolibre.view.adapter

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.mercadolibre.repo.ItemsRepository
import com.mercadolibre.repo.NetworkState
import com.mercadolibre.repo.Resource
import com.mercadolibre.repo.search.Item


/**
 * Class reponsible for transform a repository of items in a page form.
 * If something works wrong if repository inform the networkLiveData a error in
 * communication to show information to the user.
 */
class PageKeyedItemDataSource(
    private val repository: ItemsRepository,
    private val query: String,
    private val networkData: MutableLiveData<NetworkState>
) : PageKeyedDataSource<Int, Item>() {

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {}

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        val items: Resource<List<Item>> = repository.search(query, params.key)

        when (items.status) {
            Resource.Status.SUCCESS -> {
                items.data?.let {
                    callback.onResult(items.data, params.key + 1)
                } ?: kotlin.run {
                    callback.onResult(emptyList(), params.key)
                }
            }
            else -> {
                networkData.postValue(NetworkState.error(items.message))
            }
        }
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Item>
    ) {

        networkData.postValue(NetworkState.LOADING)
        val items = repository.search(query, 0)

        when (items.status) {
            Resource.Status.SUCCESS -> {
                items.data?.let {
                    callback.onResult(items.data, 0, items.data.size, null, 1)
                } ?: kotlin.run {
                    callback.onResult(emptyList(), 0, 0, null, 0)
                }
                networkData.postValue(NetworkState.LOADED)
            }
            else -> {
                networkData.postValue(NetworkState.error(items.message))
            }
        }

    }
}