package com.mercadolibre.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.navArgs
import com.bumptech.glide.Glide
import com.mercadolibre.R
import com.mercadolibre.repo.ItemsRepositoryImpl
import com.mercadolibre.viewmodel.ItemViewModel
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * Class responsible for show the details of a item for the user
 */
class DetailActivity : AppCompatActivity() {

    val args: DetailActivityArgs by navArgs()
    val viewModel: ItemViewModel by viewModel()

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.activity_detail)

        setSupportActionBar(toolbar)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        this.viewModel.item.observe(this, Observer { item ->
            Glide.with(baseContext).load(item.thumbnail?.replace("-I.jpg", "-O.jpg")).into(image)

            val condition = when (item.condition) {
                "new" -> getString(R.string.item_new_description)
                "used" -> getString(R.string.item_used_description)
                else -> {
                    Timber.w("ItemQuery condition (%s) not found return default new", item.condition)
                    getString(R.string.item_new_description)
                }
            }

            header.text =
                getString(R.string.item_header_description, condition.capitalize(), item.soldQuantity ?: 0)

            itemTitle.text = item.title

            price.text = item.getPriceDescription()

            installments.text = item.getInstallmentsDescription()

        })

        this.viewModel.item.postValue(args.item)

        this.buyButton.setOnClickListener {
            val url = this.viewModel.item.value?.permalink ?: getString(R.string.mercadolivre_link)
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }

        this.viewModel.details.observe(this, Observer {
            description.text = it?.plain_text
        })

        supportActionBar?.title = ""
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }
}
