package com.mercadolibre.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager.VERTICAL
import com.google.android.material.snackbar.Snackbar
import com.mercadolibre.R
import com.mercadolibre.repo.Status
import com.mercadolibre.view.adapter.SearchItemAdapter
import com.mercadolibre.viewmodel.SearchViewModel
import kotlinx.android.synthetic.main.fragment_search.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Class responsible to show a list of elements and network errors to the user
 */
class SearchFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    val searchViewModel: SearchViewModel by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { context: Context ->
            val adapter = SearchItemAdapter(context)
            recycleView.adapter = adapter

            searchViewModel?.items?.observe(this, Observer {
                adapter.submitList(it)
            })
        }

        recycleView.apply {
            setHasFixedSize(true)
            layoutManager = StaggeredGridLayoutManager(2, VERTICAL)
        }

        searchViewModel?.network?.observe(this, Observer {
            when (it.status) {
                Status.RUNNING -> {
                    progress_bar.show()
                    recycleView.visibility = View.INVISIBLE
                }
                Status.FAILED -> {
                    Snackbar.make(recycleView, "Erro ao carregar as informações", Snackbar.LENGTH_LONG)
                        .setAction("CLOSE") { }
                        .setActionTextColor(resources.getColor(android.R.color.holo_red_light))
                        .show()
                    progress_bar.hide()
                    recycleView.visibility = View.INVISIBLE
                }
                Status.SUCCESS -> {
                    progress_bar.hide()
                    recycleView.visibility = View.VISIBLE
                }
            }
        })

    }
}



