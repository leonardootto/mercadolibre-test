package com.mercadolibre.view

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.mercadolibre.R
import com.mercadolibre.viewmodel.SearchViewModel
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.activity_search.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * Class responsible to show the search screen to the user and keep searches in a stack way
 *
 * Disclaimer: We could use only fragments to keep this state but need further investigation
 *               in combination of Architecture Navigation Components + Fragment + Paged RecycleView + Glide
 *               This combination work's but sometimes in transition of state between searches we lost the list
 *               exactly scroll position, create a annoying experience for the user. This is the reason why we
 *               choose keep the state in Activity.
 *               The reason probably because we can't know the exactly size of the item before show the list.
 *               TODO Jira XXX
 *
 */
class SearchActivity : AppCompatActivity() {

    val searchViewModel: SearchViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.i("onCreate SearchActivity")

        setContentView(R.layout.activity_search)
        setSupportActionBar(toolbar)

        search_view.setVoiceSearch(false)
        search_view.setEllipsize(true)
        search_view.setSubmitOnClick(true)

        searchViewModel.queryHistoryLiveData.observe(this, Observer {
            Timber.d("Set query history:$it")
            search_view.setSuggestions(it.toTypedArray())
        })

        search_view.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                Timber.d("Query text:${query}")

                val intent = Intent(this@SearchActivity, SearchActivity::class.java)
                intent.apply {
                    putExtra("query", query)
                }
                startActivity(intent)

                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })

        search_view.setOnItemClickListener { parent, view, position, id ->
            Timber.d("onClick item position:$id, closing search view")
            search_view.closeSearch()
        }

        intent?.extras?.apply {
            val query = getString("query")
            Timber.d("Query ($query) passed for this activity start search ")

            searchViewModel.query.value = query

            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)

            supportActionBar?.title = query
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)

        val item = menu.findItem(R.id.action_search)
        search_view.setMenuItem(item)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }

}