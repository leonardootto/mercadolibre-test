package com.mercadolibre.repo

import com.mercadolibre.repo.desc.DescriptionResult
import com.mercadolibre.repo.item.ItemResult
import com.mercadolibre.repo.search.Item
import com.mercadolibre.repo.search.SearchResult
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import timber.log.Timber

/**
 * Interface of search items
 */
interface ItemsRepository {

    fun search(query: String, page: Int): Resource<List<Item>>
    fun item(itemId: String): Resource<ItemResult?>
    fun description(id: String): Resource<DescriptionResult?>
}


/**
 * The defaul size for the number of items in a page
 */
const val PAGE_SIZE = 20


/**
 * The retrofit interface for Meli webservices
 */
interface MeliWebService {

    @GET("sites/MLU/search")
    fun search(
        @retrofit2.http.Query("q") query: String,
        @retrofit2.http.Query("offset") offset: Int = 0,
        @retrofit2.http.Query("limit") limit: Int = PAGE_SIZE
    ): Call<SearchResult>

    @GET("items/{item_id}/")
    fun item(
        @Path("item_id") itemId: String
    ): Call<ItemResult>

    @GET("items/{item_id}/description")
    fun description(
        @Path("item_id") itemId: String
    ): Call<DescriptionResult>

}

/**
 * Concrete implementation of Items Repository
 */
class ItemsRepositoryImpl constructor
    (private val service: MeliWebService) : ItemsRepository {

    override fun search(query: String, page: Int): Resource<List<Item>> {
        Timber.d("Get itens for query:%s and page:%d", query, page)
        try {
            val response: Response<SearchResult> = this.service.search(query, page * PAGE_SIZE).execute()

            return if (response.isSuccessful) {
                val list: List<Item> = response.body()?.items?.let {
                    it
                } ?: run {
                    emptyList<Item>()
                }
                Resource.success(list)
            } else {
                Timber.e("Server return error ${response.code()} for query:%s and page:%d", query, page)
                Resource.error(response.message(), emptyList())
            }
        } catch (e: Exception) {
            Timber.e(e, "Error getting itens for query:%s and page:%d", query, page)
            return Resource.error(e.message ?: "", emptyList())
        }
    }

    override fun item(itemId: String): Resource<ItemResult?> {
        Timber.d("Get iten for Id:$itemId")
        try {
            val response: Response<ItemResult> = this.service.item(itemId).execute()
            return if (response.isSuccessful) {
                Resource.success(response.body())
            } else {
                Timber.e("Server return error ${response.code()} for iten for Id:$itemId")
                Resource.error(response.message(), null)
            }
        } catch (e: Exception) {
            Timber.e(e, "Error getting iten for Id:$itemId")
            return Resource.error(e.message ?: "", null)
        }
    }

    override fun description(id: String): Resource<DescriptionResult?> {
        Timber.d("Description item -> id:%s", id)

        try {
            val response: Response<DescriptionResult> = this.service.description(id).execute()
            return if (response.isSuccessful) {
                Resource.success(response.body())
            } else {
                Timber.e("Server return error ${response.code()} for Description item -> id:%s", id)
                Resource.error(response.message(), null)
            }
        } catch (e: Exception) {
            Timber.e(e, "Error getting description item -> id:%s", id)
            return Resource.error(e.message ?: "", null)
        }

    }
}



