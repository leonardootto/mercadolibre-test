/**
 * This file represents all models from Search api
 */
package com.mercadolibre.repo.search

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import org.joda.money.CurrencyUnit
import org.joda.money.Money
import org.joda.money.format.MoneyFormatterBuilder
import java.math.RoundingMode

class SearchResult {

    @SerializedName("available_filters")
    var availableFilters: List<AvailableFilter>? = null
    @SerializedName("available_sorts")
    var availableSorts: List<AvailableSort>? = null
    @SerializedName("filters")
    var filters: List<Filter>? = null
    @SerializedName("paging")
    var paging: Paging? = null
    @SerializedName("query")
    var query: String? = null
    @SerializedName("related_results")
    var relatedResults: List<Any>? = null
    @SerializedName("results")
    var items: List<Item>? = null
    @SerializedName("secondary_results")
    var secondaryResults: List<Any>? = null

    @SerializedName("site_id")
    var siteId: String? = null

    @SerializedName("sort")
    var sort: Sort? = null

}

class Item() : Parcelable {

    @SerializedName("accepts_mercadopago")
    var acceptsMercadopago: Boolean? = null
    @SerializedName("address")
    var address: Address? = null
    @SerializedName("attributes")
    var attributes: List<Attribute>? = null
    @SerializedName("available_quantity")
    var availableQuantity: Long? = null
    @SerializedName("buying_mode")
    var buyingMode: String? = null
    @SerializedName("catalog_product_id")
    var catalogProductId: Any? = null
    @SerializedName("category_id")
    var categoryId: String? = null
    @SerializedName("condition")
    var condition: String? = null
    @SerializedName("currency_id")
    var currencyId: String? = null
    @SerializedName("id")
    var id: String? = null
    @SerializedName("installments")
    var installments: Installments? = null
    @SerializedName("listing_type_id")
    var listingTypeId: String? = null
    @SerializedName("official_store_id")
    var officialStoreId: Any? = null
    @SerializedName("original_price")
    var originalPrice: Any? = null
    @SerializedName("permalink")
    var permalink: String? = null
    @SerializedName("price")
    var price: Float? = null

    @SerializedName("reviews")
    var reviews: Reviews? = null
    @SerializedName("seller")
    var seller: Seller? = null
    @SerializedName("seller_address")
    var sellerAddress: SellerAddress? = null
    @SerializedName("shipping")
    var shipping: Shipping? = null
    @SerializedName("site_id")
    var siteId: String? = null
    @SerializedName("sold_quantity")
    var soldQuantity: Long? = null
    @SerializedName("stop_time")
    var stopTime: String? = null
    @SerializedName("tags")
    var tags: List<String>? = null
    @SerializedName("thumbnail")
    var thumbnail: String? = null
    @SerializedName("title")
    var title: String? = null

    constructor(parcel: Parcel) : this() {
        acceptsMercadopago = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        address = parcel.readParcelable(Address::class.java.classLoader)
        attributes = parcel.createTypedArrayList(Attribute)
        availableQuantity = parcel.readValue(Long::class.java.classLoader) as? Long
        buyingMode = parcel.readString()
        categoryId = parcel.readString()
        condition = parcel.readString()
        currencyId = parcel.readString()
        id = parcel.readString()
        installments = parcel.readParcelable(Installments::class.java.classLoader)
        listingTypeId = parcel.readString()
        permalink = parcel.readString()
        price = parcel.readValue(Float::class.java.classLoader) as? Float
        reviews = parcel.readParcelable(Reviews::class.java.classLoader)
        seller = parcel.readParcelable(Seller::class.java.classLoader)
        sellerAddress = parcel.readParcelable(SellerAddress::class.java.classLoader)
        shipping = parcel.readParcelable(Shipping::class.java.classLoader)
        siteId = parcel.readString()
        soldQuantity = parcel.readValue(Long::class.java.classLoader) as? Long
        stopTime = parcel.readString()
        tags = parcel.createStringArrayList()
        thumbnail = parcel.readString()
        title = parcel.readString()
    }

    override fun toString(): String {
        return "ItemQuery(title=$title)"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(acceptsMercadopago)
        parcel.writeParcelable(address, flags)
        parcel.writeTypedList(attributes)
        parcel.writeValue(availableQuantity)
        parcel.writeString(buyingMode)
        parcel.writeString(categoryId)
        parcel.writeString(condition)
        parcel.writeString(currencyId)
        parcel.writeString(id)
        parcel.writeParcelable(installments, flags)
        parcel.writeString(listingTypeId)
        parcel.writeString(permalink)
        parcel.writeValue(price)
        parcel.writeParcelable(reviews, flags)
        parcel.writeParcelable(seller, flags)
        parcel.writeParcelable(sellerAddress, flags)
        parcel.writeParcelable(shipping, flags)
        parcel.writeString(siteId)
        parcel.writeValue(soldQuantity)
        parcel.writeString(stopTime)
        parcel.writeStringList(tags)
        parcel.writeString(thumbnail)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    private val formatterBuilder = MoneyFormatterBuilder()
        .appendCurrencySymbolLocalized()
        .appendLiteral(" ")
        .appendAmountLocalized()
        .toFormatter()

    fun getPriceDescription(): String {
        val currency = currencyId?.let { CurrencyUnit.of(currencyId) } ?: CurrencyUnit.of("UYU")
        val money = Money.of(currency, price?.toDouble() ?: 0.0, RoundingMode.HALF_EVEN)
        return formatterBuilder.print(money)
    }

    fun getInstallmentsDescription(): CharSequence? {
        val currency =
            installments?.currencyId?.let { CurrencyUnit.of(installments?.currencyId) } ?: CurrencyUnit.of("UYU")
        val installmentsMoney = Money.of(currency, installments?.amount ?: 0.0)
        val installmentsText = formatterBuilder.print(installmentsMoney)
        return "${installments?.quantity}x $installmentsText"
    }

    companion object CREATOR : Parcelable.Creator<Item> {
        override fun createFromParcel(parcel: Parcel): Item {
            return Item(parcel)
        }

        override fun newArray(size: Int): Array<Item?> {
            return arrayOfNulls(size)
        }
    }

}


class Reviews() : Parcelable {

    @SerializedName("rating_average")
    var ratingAverage: Double? = null
    @SerializedName("total")
    var total: Long? = null

    constructor(parcel: Parcel) : this() {
        ratingAverage = parcel.readValue(Double::class.java.classLoader) as? Double
        total = parcel.readValue(Long::class.java.classLoader) as? Long
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(ratingAverage)
        parcel.writeValue(total)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Reviews> {
        override fun createFromParcel(parcel: Parcel): Reviews {
            return Reviews(parcel)
        }

        override fun newArray(size: Int): Array<Reviews?> {
            return arrayOfNulls(size)
        }
    }

}


class Seller() : Parcelable {

    @SerializedName("car_dealer")
    var carDealer: Boolean? = null
    @SerializedName("id")
    var id: Long? = null
    @SerializedName("power_seller_status")
    var powerSellerStatus: String? = null
    @SerializedName("real_estate_agency")
    var realEstateAgency: Boolean? = null
    @SerializedName("tags")
    var tags: List<String>? = null

    constructor(parcel: Parcel) : this() {
        carDealer = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        id = parcel.readValue(Long::class.java.classLoader) as? Long
        powerSellerStatus = parcel.readString()
        realEstateAgency = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        tags = parcel.createStringArrayList()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(carDealer)
        parcel.writeValue(id)
        parcel.writeString(powerSellerStatus)
        parcel.writeValue(realEstateAgency)
        parcel.writeStringList(tags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Seller> {
        override fun createFromParcel(parcel: Parcel): Seller {
            return Seller(parcel)
        }

        override fun newArray(size: Int): Array<Seller?> {
            return arrayOfNulls(size)
        }
    }

}


class SellerAddress() : Parcelable {

    @SerializedName("address_line")
    var addressLine: String? = null
    @SerializedName("city")
    var city: City? = null
    @SerializedName("comment")
    var comment: String? = null
    @SerializedName("country")
    var country: Country? = null
    @SerializedName("id")
    var id: String? = null
    @SerializedName("latitude")
    var latitude: String? = null
    @SerializedName("longitude")
    var longitude: String? = null
    @SerializedName("state")
    var state: State? = null
    @SerializedName("zip_code")
    var zipCode: String? = null

    constructor(parcel: Parcel) : this() {
        addressLine = parcel.readString()
        city = parcel.readParcelable(City::class.java.classLoader)
        comment = parcel.readString()
        country = parcel.readParcelable(Country::class.java.classLoader)
        id = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        state = parcel.readParcelable(State::class.java.classLoader)
        zipCode = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(addressLine)
        parcel.writeParcelable(city, flags)
        parcel.writeString(comment)
        parcel.writeParcelable(country, flags)
        parcel.writeString(id)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeParcelable(state, flags)
        parcel.writeString(zipCode)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SellerAddress> {
        override fun createFromParcel(parcel: Parcel): SellerAddress {
            return SellerAddress(parcel)
        }

        override fun newArray(size: Int): Array<SellerAddress?> {
            return arrayOfNulls(size)
        }
    }

}


class Shipping() : Parcelable {

    @SerializedName("free_shipping")
    var freeShipping: Boolean? = null
    @SerializedName("logistic_type")
    var logisticType: String? = null
    @SerializedName("mode")
    var mode: String? = null
    @SerializedName("store_pick_up")
    var storePickUp: Boolean? = null
    @SerializedName("tags")
    var tags: List<String>? = null

    constructor(parcel: Parcel) : this() {
        freeShipping = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        logisticType = parcel.readString()
        mode = parcel.readString()
        storePickUp = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        tags = parcel.createStringArrayList()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(freeShipping)
        parcel.writeString(logisticType)
        parcel.writeString(mode)
        parcel.writeValue(storePickUp)
        parcel.writeStringList(tags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Shipping> {
        override fun createFromParcel(parcel: Parcel): Shipping {
            return Shipping(parcel)
        }

        override fun newArray(size: Int): Array<Shipping?> {
            return arrayOfNulls(size)
        }
    }

}


class Sort() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Sort> {
        override fun createFromParcel(parcel: Parcel): Sort {
            return Sort(parcel)
        }

        override fun newArray(size: Int): Array<Sort?> {
            return arrayOfNulls(size)
        }
    }

}


class State() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<State> {
        override fun createFromParcel(parcel: Parcel): State {
            return State(parcel)
        }

        override fun newArray(size: Int): Array<State?> {
            return arrayOfNulls(size)
        }
    }

}


class Value() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("path_from_root")
    var pathFromRoot: List<PathFromRoot>? = null
    @SerializedName("items")
    var results: Long? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        pathFromRoot = parcel.createTypedArrayList(PathFromRoot)
        results = parcel.readValue(Long::class.java.classLoader) as? Long
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeTypedList(pathFromRoot)
        parcel.writeValue(results)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Value> {
        override fun createFromParcel(parcel: Parcel): Value {
            return Value(parcel)
        }

        override fun newArray(size: Int): Array<Value?> {
            return arrayOfNulls(size)
        }
    }

}


class Address() : Parcelable {

    @SerializedName("city_id")
    var cityId: String? = null
    @SerializedName("city_name")
    var cityName: String? = null
    @SerializedName("state_id")
    var stateId: String? = null
    @SerializedName("state_name")
    var stateName: String? = null

    constructor(parcel: Parcel) : this() {
        cityId = parcel.readString()
        cityName = parcel.readString()
        stateId = parcel.readString()
        stateName = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(cityId)
        parcel.writeString(cityName)
        parcel.writeString(stateId)
        parcel.writeString(stateName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Address> {
        override fun createFromParcel(parcel: Parcel): Address {
            return Address(parcel)
        }

        override fun newArray(size: Int): Array<Address?> {
            return arrayOfNulls(size)
        }
    }

}

class AvailableFilter() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("values")
    var values: List<Value>? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        type = parcel.readString()
        values = parcel.createTypedArrayList(Value)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(type)
        parcel.writeTypedList(values)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AvailableFilter> {
        override fun createFromParcel(parcel: Parcel): AvailableFilter {
            return AvailableFilter(parcel)
        }

        override fun newArray(size: Int): Array<AvailableFilter?> {
            return arrayOfNulls(size)
        }
    }

}


class AvailableSort() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AvailableSort> {
        override fun createFromParcel(parcel: Parcel): AvailableSort {
            return AvailableSort(parcel)
        }

        override fun newArray(size: Int): Array<AvailableSort?> {
            return arrayOfNulls(size)
        }
    }

}


class City() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<City> {
        override fun createFromParcel(parcel: Parcel): City {
            return City(parcel)
        }

        override fun newArray(size: Int): Array<City?> {
            return arrayOfNulls(size)
        }
    }

}


class Country() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Country> {
        override fun createFromParcel(parcel: Parcel): Country {
            return Country(parcel)
        }

        override fun newArray(size: Int): Array<Country?> {
            return arrayOfNulls(size)
        }
    }

}


class Filter() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("values")
    var values: List<Value>? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        type = parcel.readString()
        values = parcel.createTypedArrayList(Value)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(type)
        parcel.writeTypedList(values)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Filter> {
        override fun createFromParcel(parcel: Parcel): Filter {
            return Filter(parcel)
        }

        override fun newArray(size: Int): Array<Filter?> {
            return arrayOfNulls(size)
        }
    }

}


class Installments() : Parcelable {

    @SerializedName("amount")
    var amount: Double? = null
    @SerializedName("currency_id")
    var currencyId: String? = null
    @SerializedName("quantity")
    var quantity: Long? = null
    @SerializedName("rate")
    var rate: Long? = null

    constructor(parcel: Parcel) : this() {
        amount = parcel.readValue(Double::class.java.classLoader) as? Double
        currencyId = parcel.readString()
        quantity = parcel.readValue(Long::class.java.classLoader) as? Long
        rate = parcel.readValue(Long::class.java.classLoader) as? Long
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(amount)
        parcel.writeString(currencyId)
        parcel.writeValue(quantity)
        parcel.writeValue(rate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Installments> {
        override fun createFromParcel(parcel: Parcel): Installments {
            return Installments(parcel)
        }

        override fun newArray(size: Int): Array<Installments?> {
            return arrayOfNulls(size)
        }
    }

}


class Paging() : Parcelable {

    @SerializedName("limit")
    var limit: Long? = null
    @SerializedName("offset")
    var offset: Long? = null
    @SerializedName("primary_results")
    var primaryResults: Long? = null
    @SerializedName("total")
    var total: Long? = null

    constructor(parcel: Parcel) : this() {
        limit = parcel.readValue(Long::class.java.classLoader) as? Long
        offset = parcel.readValue(Long::class.java.classLoader) as? Long
        primaryResults = parcel.readValue(Long::class.java.classLoader) as? Long
        total = parcel.readValue(Long::class.java.classLoader) as? Long
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(limit)
        parcel.writeValue(offset)
        parcel.writeValue(primaryResults)
        parcel.writeValue(total)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Paging> {
        override fun createFromParcel(parcel: Parcel): Paging {
            return Paging(parcel)
        }

        override fun newArray(size: Int): Array<Paging?> {
            return arrayOfNulls(size)
        }
    }

}


class PathFromRoot() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PathFromRoot> {
        override fun createFromParcel(parcel: Parcel): PathFromRoot {
            return PathFromRoot(parcel)
        }

        override fun newArray(size: Int): Array<PathFromRoot?> {
            return arrayOfNulls(size)
        }
    }

}


class Attribute() : Parcelable {

    @SerializedName("attribute_group_id")
    var attributeGroupId: String? = null
    @SerializedName("attribute_group_name")
    var attributeGroupName: String? = null
    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("source")
    var source: Long? = null
    @SerializedName("value_id")
    var valueId: String? = null
    @SerializedName("value_name")
    var valueName: String? = null
    @SerializedName("value_struct")
    var valueStruct: Any? = null

    constructor(parcel: Parcel) : this() {
        attributeGroupId = parcel.readString()
        attributeGroupName = parcel.readString()
        id = parcel.readString()
        name = parcel.readString()
        source = parcel.readValue(Long::class.java.classLoader) as? Long
        valueId = parcel.readString()
        valueName = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(attributeGroupId)
        parcel.writeString(attributeGroupName)
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeValue(source)
        parcel.writeString(valueId)
        parcel.writeString(valueName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Attribute> {
        override fun createFromParcel(parcel: Parcel): Attribute {
            return Attribute(parcel)
        }

        override fun newArray(size: Int): Array<Attribute?> {
            return arrayOfNulls(size)
        }
    }

}