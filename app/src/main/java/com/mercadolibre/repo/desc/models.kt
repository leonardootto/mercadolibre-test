package com.mercadolibre.repo.desc

/**
 * This file represents all models from Details Api
 */

class DescriptionResult {
    var text: String? = null
    var plain_text: String? = null
    var last_updated: String? = null
    var date_created: String? = null
    var snapshot: Snapshot? = null
}

class Snapshot {
    var url: String? = null
    var width: Float = 0.toFloat()
    var height: Float = 0.toFloat()
    var status: String? = null
}