package com.mercadolibre.repo.item

/**
 * This file represents all models from Item api
 */
class ItemResult {

    var id: String? = null
    var site_id: String? = null
    var title: String? = null
    var subtitle: String? = null
    var seller_id: Float = 0.toFloat()
    var category_id: String? = null
    var official_store_id: String? = null
    var price: Float = 0.toFloat()
    var base_price: Float = 0.toFloat()
    var original_price: String? = null
    var currency_id: String? = null
    var initial_quantity: Float = 0.toFloat()
    var available_quantity: Float = 0.toFloat()
    var sold_quantity: Float = 0.toFloat()
    internal var sale_terms = ArrayList<Any>()
    var buying_mode: String? = null
    var listing_type_id: String? = null
    var start_time: String? = null
    var stop_time: String? = null
    var condition: String? = null
    var permalink: String? = null
    var thumbnail: String? = null
    var secure_thumbnail: String? = null
    internal var pictures = ArrayList<Any>()
    var video_id: String? = null
    internal var descriptions = ArrayList<Any>()
    var accepts_mercadopago: Boolean = false
    internal var non_mercado_pago_payment_methods = ArrayList<Any>()
    var shipping: Shipping? = null
    var international_delivery_mode: String? = null
    var seller_address: Seller_address? = null
    var seller_contact: String? = null
    var location: Location? = null
    var geolocation: Geolocation? = null
    internal var coverage_areas = ArrayList<Any>()
    internal var attributes = ArrayList<Any>()
    internal var warnings = ArrayList<Any>()
    var listing_source: String? = null
    internal var variations = ArrayList<Any>()
    var status: String? = null
    internal var sub_status = ArrayList<Any>()
    internal var tags = ArrayList<Any>()
    var warranty: String? = null
    var catalog_product_id: String? = null
    var domain_id: String? = null
    var parent_item_id: String? = null
    var differential_pricing: String? = null
    internal var deal_ids = ArrayList<Any>()
    var automatic_relist: Boolean = false
    var date_created: String? = null
    var last_updated: String? = null
    var health: Float = 0.toFloat()
}

class Geolocation {
    var latitude: Float = 0.toFloat()
    var longitude: Float = 0.toFloat()
}

class Location// Getter Methods
// Setter Methods
class Seller_address {
    // Getter Methods

    // Setter Methods

    var comment: String? = null
    var address_line: String? = null
    var zip_code: String? = null
    var city: City? = null
    var state: State? = null
    var country: Country? = null
    var search_location: Search_location? = null
    var latitude: Float = 0.toFloat()
    var longitude: Float = 0.toFloat()
    var id: Float = 0.toFloat()
}

class Search_location {
    // Getter Methods

    // Setter Methods

    var neighborhood: Neighborhood? = null
    var city: City? = null
    var state: State? = null
}

class State {
    // Getter Methods

    // Setter Methods

    var id: String? = null
    var name: String? = null
}

class City {
    // Getter Methods

    // Setter Methods

    var id: String? = null
    var name: String? = null
}

class Neighborhood {
    // Getter Methods

    // Setter Methods

    var id: String? = null
    var name: String? = null
}

class Country {
    // Getter Methods

    // Setter Methods

    var id: String? = null
    var name: String? = null
}

class Shipping {
    // Getter Methods

    // Setter Methods

    var mode: String? = null
    internal var methods = ArrayList<Any>()
    internal var tags = ArrayList<Any>()
    var dimensions: String? = null
    var local_pick_up: Boolean = false
    var free_shipping: Boolean = false
    var logistic_type: String? = null
    var store_pick_up: Boolean = false
}