package com.mercadolibre.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import com.mercadolibre.repo.ItemsRepository
import com.mercadolibre.repo.NetworkState
import com.mercadolibre.repo.Resource
import com.mercadolibre.repo.Resource.Status.*
import com.mercadolibre.repo.desc.DescriptionResult
import com.mercadolibre.repo.item.ItemResult
import com.mercadolibre.repo.search.Item


/**
 * Class responsible for keep the state in android components architectures and keep items details
 */
class ItemViewModel(val repository: ItemsRepository) : ViewModel() {

    val item = MutableLiveData<Item>()

    val network = MutableLiveData<NetworkState>()

    val details: LiveData<DescriptionResult?> = map(item) {
        it?.id?.let {
            val result = repository.description(it)

            when (result) {
                SUCCESS -> {
                    network.postValue(NetworkState.LOADED)
                    result.data
                }
                ERROR -> {
                    network.postValue(NetworkState.error(result.message))
                    null
                }
                LOADING -> {
                    network.postValue(NetworkState.LOADING)
                    result.data
                }
                else -> {
                    null
                }
            }
        }
    }
}
