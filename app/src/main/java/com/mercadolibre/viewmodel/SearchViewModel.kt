package com.mercadolibre.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.mercadolibre.repo.ItemsRepository
import com.mercadolibre.repo.NetworkState
import com.mercadolibre.repo.PAGE_SIZE
import com.mercadolibre.repo.search.Item
import com.mercadolibre.view.adapter.PageKeyedItemDataSource

data class Listing<T>(val pagedList: LiveData<PagedList<T>>)


/**
 * Class responsible for keep the state in android components architectures and perform
 * query search to items transformations.
 */
class SearchViewModel constructor(val repository: ItemsRepository) : ViewModel() {

    private val queryHistory = ArrayList<String>()
    val queryHistoryLiveData = MutableLiveData<List<String>>()

    val query = MutableLiveData<String>()

    val network = MutableLiveData<NetworkState>()

    private val results: LiveData<Listing<Item>> = map(query) { search: String ->

        queryHistory.add(search)
        queryHistoryLiveData.postValue(queryHistory)

        val factory = object : DataSource.Factory<Int, Item>() {
            override fun create(): DataSource<Int, Item> {
                return PageKeyedItemDataSource(repository, search, network)
            }
        }
        val toLiveData: LiveData<PagedList<Item>> = factory.toLiveData(PAGE_SIZE)
        Listing(toLiveData)
    }

    val items = switchMap(results) { it?.pagedList }!!

}