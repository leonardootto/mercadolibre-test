package com.mercadolibre

import android.app.Application
import com.mercadolibre.repo.ItemsRepository
import com.mercadolibre.repo.ItemsRepositoryImpl
import com.mercadolibre.repo.MeliWebService
import com.mercadolibre.viewmodel.ItemViewModel
import com.mercadolibre.viewmodel.SearchViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.android.startKoin
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

/**
 * This is koin appModule with all beans
 */
val appModule = module {

    //Repository Related
    single<ItemsRepository> { ItemsRepositoryImpl(get()) }
    single(name = "api.url") { "https://api.mercadolibre.com/" }
    single<MeliWebService> {
        val retrofit = Retrofit.Builder()
            .baseUrl(get<String>(name = "api.url"))
            .addConverterFactory(GsonConverterFactory.create())
            .client(get())
            .build()
        retrofit.create(MeliWebService::class.java)
    }
    single { OkHttpClient() }

    //View Model related
    viewModel {
        SearchViewModel(get())
    }
    viewModel {
        ItemViewModel(get())
    }
}

/**
 * Application class extensions.
 * Used to configure some environment configuration (logs, injections, etc)
 */
class MercadoLibreApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        startKoin(this, listOf(appModule))
    }

}
