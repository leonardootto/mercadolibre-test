package com.mercadolibre.repo

import com.mercadolibre.appModule
import com.mercadolibre.repo.search.Item
import org.junit.Assert.assertTrue
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.get
import org.koin.test.KoinTest
import org.koin.test.declareMock
import org.mockito.Mockito
import org.mockserver.client.MockServerClient
import org.mockserver.junit.MockServerRule
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response


/**
 * Test module, this will overwrite definitions of application module
 */
val testModule = module {
    // single instance of HelloRepository
    single(name = "api.url", override = true) { "http://localhost:9000/" }
    single(override = true) { Mockito.mock(SearchDao::class.java) }
}


/**
 * Test class for ItemRepository
 */
class ItemsRepositoryTest : KoinTest {

    /**
     * Static method for initalize koin
     */
    companion object {
        @JvmStatic
        @BeforeClass
        fun setup() {
            startKoin(listOf(appModule, testModule))
        }
    }

    /**
     * We choose use a mock server instead mock all repository,
     * makes the test more clean and easy
     */
    @get:Rule
    var mockServerRule = MockServerRule(this, 9000)

    lateinit var mockServerClient: MockServerClient

    /**
     * We store all returns inside a test /resource directory
     */
    private fun getResource(s: String) =
        this::class.java.getClassLoader().getResourceAsStream(s).bufferedReader().use { it.readText() }

    @Test
    fun testSearchCallOk() {


        mockServerClient.`when`(
            request()
                .withMethod("GET")
                .withPath("/sites/MLU/search")
                .withQueryStringParameter("q", "chrome")
                .withQueryStringParameter("offset", "0")
                .withQueryStringParameter("limit", "20")
        ).respond {
            response().withStatusCode(200).withBody(getResource("search1.json"))
        }

        var repo: ItemsRepository = get()
        val itens: Resource<List<Item>> = repo.search("chrome", 0)
        assertTrue(itens.status == Resource.Status.SUCCESS)
        assertTrue(itens.data?.size == 20)
    }

    @Test
    fun testSearchCallFail() {
        mockServerClient.`when`(
            request()
                .withMethod("GET")
                .withPath("/sites/MLU/search")
                .withQueryStringParameter("q", "chrome")
                .withQueryStringParameter("offset", "0")
                .withQueryStringParameter("limit", "20")
        ).respond {
            response().withStatusCode(500)
        }

        var repo: ItemsRepository = get()
        val itens: Resource<List<Item>> = repo.search("chrome", 0)
        assertTrue(itens.status == Resource.Status.ERROR)
    }

    @Test
    fun testSearchCallFailWrongFormat() {
        mockServerClient.`when`(
            request()
                .withMethod("GET")
                .withPath("/sites/MLU/search")
                .withQueryStringParameter("q", "chrome")
                .withQueryStringParameter("offset", "0")
                .withQueryStringParameter("limit", "20")
        ).respond {
            response().withStatusCode(200).withBody(getResource("wrongformat.json"))
        }

        var repo: ItemsRepository = get()
        val itens: Resource<List<Item>> = repo.search("chrome", 0)
        assertTrue(itens.status == Resource.Status.ERROR)
    }


    @Test
    fun testItemCall() {
        mockServerClient.`when`(
            request().withPath("/items/MLU459152780/")
        ).respond(
            response().withStatusCode(200).withBody(getResource("item.json"))
        )

        var repo: ItemsRepository = get()
        val item = repo.item("MLU459152780")

        assertTrue(item.status == Resource.Status.SUCCESS)
        assertTrue(item.data?.id == "MLU459152780")
    }

    @Test
    fun testItemCallWrongFormat() {
        mockServerClient.`when`(
            request().withPath("/items/MLU459152780/")
        ).respond(
            response().withStatusCode(200).withBody(getResource("wrongformat.json"))
        )

        var repo: ItemsRepository = get()
        val item = repo.item("MLU459152780")

        assertTrue(item.status == Resource.Status.ERROR)
    }

    @Test
    fun testItemCallFail() {
        mockServerClient.`when`(
            request().withPath("/items/MLU459152780/")
        ).respond {
            response().withStatusCode(500)
        }

        var repo: ItemsRepository = get()
        val item = repo.item("MLU459152780")

        assertTrue(item.status == Resource.Status.ERROR)
    }
}
