Meli Android Appliation
===========================================================

This is a simples application to access the Meli open Web services.

Its use the following libraries and components:

- Kotlin Language
- Android Jetpack + Architecture Components ( LiveData, ViewModel, Pagination,  )
- Material design libraries
- Retrofit to access web services
- Glide for images handling
- Koin for DI
- Timber for Logs
- Joda Money to handling money values
- JUnit and MockingService for testing


Introduction
-------------
### Functionality

This app is composed of 2 screens, Search and Details.

### Building
You can open the project in Android studio and press run.

